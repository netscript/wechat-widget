<?php
/**
 * Project Widget
 * file: TestServer.php
 * User: weblinuxgame
 * Date: 2019/5/28
 * Time: 14:03
 */

namespace WebLinuxGame\WeChat\Widget\Tests\Unit;

use WebLinuxGame\WeChat\Widget\Guard;
use WebLinuxGame\WeChat\Widget\Tests\TestCase;

/**
 * Class TestServer
 * @package WebLinuxGame\WeChat\Widget\Tests\Unit
 */
class TestServer extends TestCase
{

    /**
     * @throws \EasyWeChat\Kernel\Exceptions\BadRequestException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     */
    public function testPush()
    {
          $app =  $this->app();
          $res = $app->server->serve()->send();
          $this->assertSame( 'success', $res->getContent(),'调用异常');
    }

    public function testServer()
    {
        $app =  $this->app();
        $server = $app->server;
        $this->assertTrue( $server instanceof Guard,'注入对象不一致');
    }

}