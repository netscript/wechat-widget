<?php
/**
 * Project Widget
 * file: TestApplaction.php
 * User: weblinuxgame
 * Date: 2019/5/27
 * Time: 12:46
 */

namespace WebLinuxGame\WeChat\Widget\Tests\Unit;

use EasyWeChat\Kernel\Support\XML;
use WebLinuxGame\WeChat\Widget\Client;
use WebLinuxGame\WeChat\Widget\ConstEnum;
use WebLinuxGame\WeChat\Widget\Application;
use WebLinuxGame\WeChat\Widget\Messages\WidgetData;
use WebLinuxGame\WeChat\Widget\Tests\TestCase;
use WebLinuxGame\WeChat\Widget\Tests\Mock\MockImportData;

/**
 * 创建应用测试
 * Class TestApplication
 * @package WebLinuxGame\WeChat\Widget\Tests\Unit
 */
class TestApplication extends TestCase
{
    /**
     * 获取模拟导入数据
     * @return MockImportData
     */
    protected function getMock()
    {
        $data = $this->import('Mock/data/jobs.php');
        return new MockImportData($data);
    }

    /**
     * 测试创建应用
     */
    public function testCreateApp()
    {
        $app = $this->app();
        $this->assertTrue($app instanceof Application, '创建应用失败');
    }

    /**
     * 测试环境变量
     */
    public function testAppEnv()
    {
        $app = $this->app();
        $this->assertSame(AppEnv('WECHAT_MINI_PROGRAM_APP_ID'), $app['config']->get('app_id'), '配置不一致');
    }

    /**
     * 测试获取widget 对象
     */
    public function testWidget()
    {
        $app = $this->app();
        $this->assertTrue($app->widget instanceof Client, '异常服务');
    }

    /**
     * 测试导入数据
     */
    public function testWidgetImport()
    {
        $app = $this->app();
        $data = $this->getMock()->generateImportOnce()->current();
        $query = ['type' => AppEnv('WIDGET_QUERY_TYPE')];
        $toUserName = AppEnv('WECHAT_MINI_PROGRAM_ROW_ID');
        $req = $app->widget->import($data, $query, $toUserName);
        $this->assertSame(['errcode' => 0, 'errmsg' => 'ok'], $req, '导入widget微信服务器失败');
    }

    /**
     * 测试加密通信查询
     */
    public function testWidgetSearch()
    {
        $app = $this->app();
        $data = $this->getMock()->generateImportOnce()->current();
        $query = ['type' => AppEnv('WIDGET_QUERY_TYPE')];
        $toUserName = AppEnv('WECHAT_MINI_PROGRAM_ROW_ID');
        $encryptData = $app[ConstEnum::PROVIDER_WIDGET]->search($data, $query, $toUserName);
        $oMessage = new WidgetData($encryptData);
        $this->assertSame($oMessage->toXmlArray(), $oMessage->toJsonArray(), '加密异常');
    }

    public function testDecode()
    {
        $data = 'GrnpWMk8gf0bYXRUx9lE4p2Y9pA5LeVvUGzVyiHSqygTM4CQmIYsSnphuBGVz9IdqIXfMlgd96Y8bpQQ6INP7gGxho91tzRzhNwq0IDmK4h72rVavLvBMQF9BZP7MAfn6lMkUMuUmiFvgSC42pDJ/KId+VVraPhqGbmwF6Q5BMSeKOxVSFRljKLDKygE7dtf4llXEf+L97YMGcnUXvpaw9brc2Vw/hiwBDsCw/aWKDoiBFHq98eef2zaz4t/IpQu9svtj/W6HmRGBuEnN757v7XWy76T6lZLLLoh5SK3FL3gvj5otWAzDXlTjwo0bQx/tk8yBukZN4r9EMeXq2pwlcb62EvakYyrMFNZ0zuNmtiX+7kUSqliG/O5W9cYTAIAz7Pzk+xP8XBXCs9wovpHrBYwuEa96IpiygWk6BwCY/73imy4rKzPBp2cY4bLHslztBY6mRJx6sgFRm9ZxCdTLbPz4UOpOHbGI86ecQsp4lZlSav6JqTa3rayh8EE5bWhLBLYyMmn2TSoFlLZHkgSXOx97j9cAMhfyq9aZWSEC2QqHIFGgX64sQA/O0DSMCPatnV2pEABsnI7JsjK7K9vWCtaMuIybieojeAJAQauzne00INzfmjrxFWLG3quY+NH842AfBNPOOr3HNrVjsSXaJ0vInZvSEK01TMQf1imZNpFDML4IROfs2lXwlXe4xy0uR3mVo3qMy0eueUtCWfW2gT0HNqn0eX6Mv1jn6kqPau4rbtnS1a6Y/FWMojmeHVDSCT75RgHfaJkT9x0PxELXywt6q8Fjnhp3qD4RBvcgHzy9ykhl0ISkzDoOaZ3ly7fq8MsLUyjvU7tUwIHoZVRqcDfJ/zm3Oon90cvbNj12b0SYTurgIjYok8Vi/t3qlUINgXk9YZMnq5p2SuvecNV8hCB7yyfJTzfqKwZVlNLzxdGjRsL9F0l1M5Omdz6ujfuLmSjlVeNqt3cKri1/Ig48YfFzO34S/TPv4bu5lw9fP82kjg4FyrLJbVtXJo9SjtIKUfsJprbA4g8tMnDTn7gJ1QqaB2vtGJTjHYTcCN/0gx2aC/70bb6iI+tfBKefzHzGk0VpBAZPnfFopUb6eOFIrrcid/jME7iT9ZpKKTnMTN4pkCE3OzVi0ZKBUVz1K3Aurve85WiYeurevUB81sSPWdxarO9fypY3bZJueW/3BNG6y7RIqDnX8KkqUvzPpq5BRqVtuVsCj0CyDnykb2OUK1drfVZDUJ+bbpAXppYIiK5JqAtBUqbBBhU0JHb20kyLf7XDnn2SvPmA7DIKv4HW8X21tFdwdJUzifAldrFTAO89+6gUY2uFpGvn0qGusWqoSBZuZ8WS2A4g0dLNdQXTg2miE6XKkW95ULAMLe0JjP2gH3UOU77hpQRCqSP7kWQ';
       // $row = base64_decode($data);
        // dump($row);
        $msgSign = '48581cbede5b376f1bb7733fe705fa79880408c7';
        $nonce  = 'wx225f0174';
        $timestamp = 1559717789;
        $msg = $this->app()->widget_encryptor->decrypt($data,$msgSign,$nonce,$timestamp);
        $xml = XML::parse($msg);
        dump(XML::parse($xml['Content']));
    }
}