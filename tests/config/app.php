<?php
/**
 * Project Widget
 * file: config.php
 * User: weblinuxgame
 * Date: 2019/5/27
 * Time: 11:50
 */

return [
    /*
   * 默认配置，将会合并到各模块中
   */
    'defaults' => [
        /*
         * 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
         */
        'response_type' => 'array',

        /*
         * 使用 Laravel 的缓存系统
         */
        'use_laravel_cache' => false,

        /*
         * 日志配置
         *
         * level: 日志级别，可选为：
         *                 debug/info/notice/warning/error/critical/alert/emergency
         * file：日志文件位置(绝对路径!!!)，要求可写权限
         */
        'log' => [
            'default' => 'dev', // 默认使用的 channel，生产环境可以改为下面的 prod
            'channels' => [
                // 测试环境
                'dev' => [
                    'driver' => 'single',
                    'path' => AppEnv('WECHAT_LOG_FILE', 'logs/wechat.log'),
                    'level' => 'debug',
                ],
                // 生产环境
                'prod' => [
                    'driver' => 'daily',
                    'path' => AppEnv('WECHAT_LOG_FILE', 'logs/wechat.log'),
                    'level' => 'info',
                ],
            ],
        ],
    ],

    /*
     * 小程序
     */
    'mini_program' => [
        'default' => [
            'app_id' => AppEnv('WECHAT_MINI_PROGRAM_APP_ID', ''),
            'secret' => AppEnv('WECHAT_MINI_PROGRAM_SECRET', ''),
            'token' => AppEnv('WECHAT_MINI_PROGRAM_TOKEN', ''),
            'aes_key' => AppEnv('WECHAT_MINI_PROGRAM_AES_KEY', ''),
            'widget_aes_key' => AppEnv('WECHAT_MINI_PROGRAM_AES_KEY', ''),
            'request_type' => AppEnv('WECHAT_MINI_PROGRAM_WIDGET_RET','json'),
        ],
    ],
];