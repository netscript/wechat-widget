<?php
/**
 * Project Widget
 * file: TestCase.php
 * User: weblinuxgame
 * Date: 2019/5/27
 * Time: 12:49
 */

namespace WebLinuxGame\WeChat\Widget\Tests;

use Illuminate\Support\Arr;
use Symfony\Component\Dotenv\Dotenv;
use WebLinuxGame\WeChat\Widget\Application;
use PHPUnit\Framework\TestCase as BaseTestCase;


/**
 * Class TestCase
 * @package WebLinuxGame\WeChat\Widget\Tests
 */
class TestCase extends BaseTestCase
{
    protected static $app;
    protected static $config;

    /**
     * 测试构造初始化
     * TestCase constructor.
     * @param null|string $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        $this->initEnv()->app();
        parent::__construct($name, $data, $dataName);
    }

    /**
     * 获取app
     * @return Application
     */
    public function app()
    {
        if (empty(self::$app)) {
            self::$app = new Application($this->config());
        }
        return self::$app;
    }

    /**
     * 配置获取
     * @param string|null $key
     * @param string|null $value
     * @return bool|mixed|null
     */
    protected function config(string $key = null, string $value = null)
    {
        if (empty(self::$config)) {
            $this->initEnv();
        }
        if (empty($value) && empty($key)) {
            return array_merge($this->config('defaults'), $this->config('mini_program.default'));
        }
        if (!empty($key) && empty($value)) {
            return Arr::get(self::$config, $key, null);
        }
        self::$config[$key] = $value;
        return putenv("$key=$value");
    }

    /**
     * 初始环境
     */
    protected function initEnv()
    {
        if(empty(self::$config)){
            $oDotEnv = new Dotenv();
            $oDotEnv->load(__DIR__ . '/.env');
            self::$config = $this->import('config/app.php');
        }
        return $this;
    }

    /**
     * @param string $file
     * @return mixed|null
     */
    protected function import(string $file = '')
    {
        if (empty($file)) {
            return null;
        }
        if ("\\" !== DIRECTORY_SEPARATOR) {
            $file = str_replace('\\', DIRECTORY_SEPARATOR, $file);
        }
        if (!preg_match('/^\/.+/', $file)) {
            $file = __DIR__ . DIRECTORY_SEPARATOR . $file;
        }
        if (file_exists($file) && !empty($info = pathinfo($file))) {
            if ('php' != strtolower($info['extension'])) {
                return file_get_contents($file);
            }
            return require $file;
        }
        return null;
    }

}