<?php
/**
 * Project Widget
 * file: MockImportData.php
 * User: weblinuxgame
 * Date: 2019/5/27
 * Time: 15:08
 */

namespace WebLinuxGame\WeChat\Widget\Tests\Mock;


/**
 * Class MockImportData
 * @package WebLinuxGame\WeChat\Widget\Tests\Mock
 */
class MockImportData implements \Iterator, \ArrayAccess
{
    protected $seq = 0;
    protected $data = [];
    protected $items = [];
    protected $position = 0;
    protected $attribute = [];

    /**
     * 模拟导入数据
     * MockImportData constructor.
     * @param array $attribute
     * @param array $items
     */
    public function __construct(array $items = [], array $attribute = [])
    {
        $this->items = $items;
        $this->attribute = $attribute;
    }

    /**
     * 单次导入完成
     * @return MockImportData
     */
    public function generateImportOnce(): self
    {
        $count = $total = rand(1, count($this->items) - 1);
        $this->data [] = [
            'items' => $this->getItems($total, $this->getCurrentSeq()),
            'attribute' => $this->getAttribute($total, $count),
        ];
        return $this;
    }

    /**
     * 多次导入完成
     * @param int $count
     * @param int $total
     * @return MockImportData
     */
    public function generateImportTimes(int $count = 10, int $total = 100): self
    {
        $total = $this->generateItems($total);
        if ($count > $total) {
            $count = $total;
        }
        $listArr = $this->getItems($count, $this->getCurrentSeq());
        if (empty($listArr)) {
            return $this;
        }
        foreach ($listArr as $i => $items) {
            $this->seq = $i;
            $this->data[] = [
                'items' => $items,
                'attribute' => $this->getAttribute($total, $count),
            ];
        }
        return $this;
    }

    /**
     * @param int $total
     * @return int
     */
    protected function generateItems(int $total)
    {
        $count = count($this->items);
        if (empty($total) || $total > $count) {
            $total = $count;
        }
        $this->items = array_slice($this->items, 0, $total);
        return $total;
    }

    /**
     * 导入数据
     * @param int $total
     * @param int $count
     * @return array
     */
    protected function getAttribute(int $total, int $count = 10): array
    {
        return [
            'totalcount' => $total,
            'count' => $count,
            'id' => $this->getImportId(),
            'seq' => $this->getCurrentSeq(),
        ];
    }

    /**
     * 获取导入想
     * @param int $count
     * @param int $seq
     * @return array
     */
    protected function getItems(int $count, int $seq = 0)
    {
        $tmp = array_chunk($this->items, $count);
        return $tmp[$seq] ?? [];
    }

    /**
     * 获取导入批次id
     * @return string
     */
    protected function getImportId(): string
    {
        return AppEnv('WIDGET_QUERY_PUSH_ID');
    }

    /**
     * 获取当前批次
     * @return int
     */
    public function getCurrentSeq(): int
    {
        return $this->seq;
    }

    /**
     * Return the current element
     * @link https://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current()
    {
        return current($this->data);
    }

    /**
     * Move forward to next element
     * @link https://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next()
    {
        if ($this->valid()) {
            $this->position++;
            next($this->data);
        } else {
            $this->position = 0;
        }
    }

    /**
     * Return the key of the current element
     * @link https://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key()
    {
        return key($this->data);
    }

    /**
     * Checks if current position is valid
     * @link https://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid()
    {
        return false !== next($this->data) && !is_null($this->key());
    }

    /**
     * Rewind the Iterator to the first element
     * @link https://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind()
    {
        $this->position = 0;
        reset($this->data);
    }

    /**
     * Whether a offset exists
     * @link https://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        if (isset($this->data[$offset])) {
            return true;
        }
        return false;
    }

    /**
     * Offset to retrieve
     * @link https://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        return $this->data[$offset] ?? null;
    }

    /**
     * Offset to set
     * @link https://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        $this->data[$offset] = $value;
    }

    /**
     * Offset to unset
     * @link https://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        if (isset($this[$offset])) {
            unset($this[$offset]);
        }
    }
}