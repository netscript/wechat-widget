<?php
/**
 * Project Widget
 * file: jobs.php
 * User: weblinuxgame
 * Date: 2019/5/27
 * Time: 16:27
 */

return [
    ['job_name' => '前端工程师', 'company_name' => '中通信息科技'],
    ['job_name' => 'python工程师', 'company_name' => '滕代科技'],
    ['job_name' => '后端工程师', 'company_name' => '云文科技'],
    ['job_name' => 'AI工程师', 'company_name' => '段云科技'],
    ['job_name' => '大话工程师', 'company_name' => '称号科技'],
    ['job_name' => '中级工程师', 'company_name' => '李白科技'],
    ['job_name' => 'php工程师', 'company_name' => '天奇科技'],
    ['job_name' => 'C++工程师', 'company_name' => '云书科技'],
    ['job_name' => 'golang工程师', 'company_name' => '黄河科技'],
    ['job_name' => 'java工程师', 'company_name' => '腾讯科技'],
    ['job_name' => 'ruby工程师', 'company_name' => '腾讯科技'],
    ['job_name' => 'Dart工程师', 'company_name' => '腾讯科技'],
    ['job_name' => 'kotlin工程师', 'company_name' => '腾讯科技'],
    ['job_name' => 'kotlin工程师', 'company_name' => '花生科技'],
    ['job_name' => 'kotlin工程师', 'company_name' => '海山云科技'],
];