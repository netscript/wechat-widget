<?php
/**
 * Project Widget
 * file: Message.phpp
 * User: weblinuxgame
 * Date: 2019/5/28
 * Time: 14:07
 */

namespace WebLinuxGame\WeChat\Widget\Messages;


use WebLinuxGame\WeChat\Widget\ConstEnum;
use EasyWeChat\Kernel\Messages\Message as BaseMessage;

/**
 * Class Message
 * @package WebLinuxGame\WeChat\Widget\Messages
 */
class Message extends BaseMessage
{
    /**
     * 是否widget 事件
     * @param array $message
     * @return bool
     */
    public static function isWidgetEvent(array $message): bool
    {
        if (empty($message) || empty($message[ConstEnum::KEY_MSG_EVENT]) || empty($message[ConstEnum::KEY_MSG_TYPE])) {
            return false;
        }
        $type = $message[ConstEnum::KEY_MSG_TYPE];
        $event = $message[ConstEnum::KEY_MSG_EVENT];
        if (ConstEnum::VAL_MSG_TYPE_EVENT == $type && ConstEnum::VAL_MSG_WIDGET_EVENT == $event) {
            return true;
        }
        return false;
    }

    /**
     * 获取查询数据
     * @param array $message
     * @return array
     */
    public static function getWidgetQuery(array $message): array
    {
        if (!empty($message[ConstEnum::KEY_MSG_SQL]) && is_string($message[ConstEnum::KEY_MSG_SQL])) {
            return (array)json_decode($message[ConstEnum::KEY_MSG_SQL], true);
        }
        return [];
    }

}