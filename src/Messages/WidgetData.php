<?php
/**
 * Project Widget
 * file: WidgetData.php
 * User: weblinuxgame
 * Date: 2019/6/5
 * Time: 11:47
 */

namespace WebLinuxGame\WeChat\Widget\Messages;

/**
 * Class WidgetData
 * @package WebLinuxGame\WeChat\Widget\Messages
 */
class WidgetData extends Message
{
    /**
     * Message type.
     *
     * @var string
     */
    protected $type = 'widget_data';

    /**
     * Properties.
     *
     * @var array
     */
    protected $properties = ['content'];

    /**
     * WidgetData constructor.
     *
     * @param array|string $data
     */
    public function __construct($data)
    {
        if (is_array($data)) {
            if (!empty($data['Content']) && is_array($data['Content'])) {
                $data = $data['Content'];
            }
            $data = json_encode($data, JSON_UNESCAPED_UNICODE);
        }
        if (is_object($data) && method_exists($data, 'toJson')) {
            $data = $data->toJson();
        }
        parent::__construct(['content' => (string)$data]);
    }

    /**
     * 输出
     * @return array
     */
    public function toXmlArray()
    {
        return $this->toJsonArray();
    }

    /**
     * 输出json
     * @return array
     */
    public function toJsonArray()
    {
        return [
            'Content' => $this->get('content'),
        ];
    }


}