<?php
/**
 * Project Widget
 * file: Guard.php
 * User: weblinuxgame
 * Date: 2019/5/28
 * Time: 10:53
 */

namespace WebLinuxGame\WeChat\Widget;


use EasyWeChat\Kernel\Support\XML;
use EasyWeChat\Kernel\Exceptions\BadRequestException;
use WebLinuxGame\WeChat\Widget\Exception\RuntimeException;
use EasyWeChat\OfficialAccount\Server\Guard as ServerGuard;
/**
 * Class Guard
 * @package WebLinuxGame\WeChat\Widget
 */
class Guard extends ServerGuard
{

    /**
     * @param array $message
     * @return mixed
     */
    protected function decryptMessage(array $message)
    {
        /**
         * @var Application $app
         */
        $app = $this->app;
        $message = $app[ConstEnum::PROVIDER_WIDGET_ENCRYPTOR]->decode(
            $message[ConstEnum::MSG_KEY_ENCRYPT],
            $this->app[ConstEnum::PROVIDER_REQ]->get(ConstEnum::MSG_KEY_MSG_SIGNATURE),
            $this->app[ConstEnum::PROVIDER_REQ]->get(ConstEnum::MSG_KEY_NONCE),
            $this->app[ConstEnum::PROVIDER_REQ]->get(ConstEnum::MSG_KEY_TIMESTAMP)
        );
        if (empty($message)) {
            return null;
        }
        return $message;
    }

    /**
     * 获取数据
     * @return array|mixed
     * @throws BadRequestException
     */
    public function getMessage()
    {
        $message = $this->parseMessage($this->app[ConstEnum::PROVIDER_REQ]->getContent(false));

        if (!is_array($message) || empty($message)) {
            throw new BadRequestException('No message received.');
        }

        if ($this->isSafeMode() && !empty($message[ConstEnum::MSG_KEY_ENCRYPT])) {
            $message = $this->decryptMessage($message);

            if (empty($message)) {
                return [];
            }
            // Handle JSON format.
            $dataSet = json_decode($message, true);
            if (empty($dataSet) || (JSON_ERROR_NONE !== json_last_error())) {
                return XML::parse($message);
            }
            if (!empty($dataSet[ConstEnum::KEY_QUERY])) {
                $dataSet[ConstEnum::KEY_QUERY] = json_decode($dataSet[ConstEnum::KEY_QUERY], true);
            }
            return $dataSet;
        }
        return $message;
    }


}