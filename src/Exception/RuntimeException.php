<?php
/**
 * Project Widget
 * file: WidgetExcleption.php
 * User: weblinuxgame
 * Date: 2019/5/24
 * Time: 18:21
 */

namespace WebLinuxGame\WeChat\Widget\Exception;


use Throwable;
use WebLinuxGame\WeChat\Widget\ConstEnum;

/**
 * 异常
 * Class RuntimeException
 * @package WebLinuxGame\WeChat
 */
class RuntimeException extends \Exception
{

    /**
     * 构造
     * RuntimeException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * 空调用错误
     * @param string $message
     * @return RuntimeException
     */
    public static function EmptyPush(string $message)
    {
        return new static($message, ConstEnum::ERR_CODE_EMPTY_DATA_PUSH);
    }

    /**
     * 查询参数异常
     * @param string $message
     * @return RuntimeException
     */
    public static function ErrorQuery(string $message)
    {
        return new static($message, ConstEnum::ERR_CODE_ERROR_PARAM_QUERY);
    }

}