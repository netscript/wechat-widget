<?php
/**
 * Project Widget
 * file: Application.php
 * User: weblinuxgame
 * Date: 2019/5/27
 * Time: 11:33
 */

namespace WebLinuxGame\WeChat\Widget;

use EasyWeChat\MiniProgram\Application as BaseApplication;

/**
 *  拓展微信小程序应用主体
 * Class Application
 * @package WebLinuxGame\WeChat\Widget
 * @author weblinuxgame <weblinuxgame@foxmail.com>
 * @property \EasyWeChat\MiniProgram\Auth\AccessToken $access_token
 * @property \EasyWeChat\MiniProgram\DataCube\Client $data_cube
 * @property \EasyWeChat\MiniProgram\AppCode\Client $app_code
 * @property \EasyWeChat\MiniProgram\Auth\Client $auth
 * @property \WebLinuxGame\WeChat\Widget\Client $widget
 * @property \WebLinuxGame\WeChat\Widget\Encryptor $widget_encryptor
 * @property \WebLinuxGame\WeChat\Widget\Messenger $widget_messenger
 * @property \WebLinuxGame\WeChat\Widget\Guard $server
 * @property \EasyWeChat\MiniProgram\TemplateMessage\Client $template_message
 * @property \EasyWeChat\OfficialAccount\CustomerService\Client $customer_service
 * @property \EasyWeChat\BasicService\Media\Client $media
 */
class Application extends BaseApplication
{

    /**
     * Application constructor.
     * @param array $config
     * @param array $prepends
     * @param null|string $id
     */
    public function __construct(array $config = [], array $prepends = [], ?string $id = null)
    {
        parent::__construct($config, $prepends, $id);
        $this->registerProviders([ServiceProvider::class]);
    }
}