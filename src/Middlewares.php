<?php
/**
 * Project Widget
 * file: Middlewares.php
 * User: weblinuxgame
 * Date: 2019/5/27
 * Time: 11:14
 */

namespace WebLinuxGame\WeChat\Widget;


use Closure;

/**
 * 请求中间
 * Class Middlewares
 * @package WebLinuxGame\WeChat\Widget
 */
class Middlewares
{

    // 中间配置容器
    protected static $middlewares = [];

    /**
     * 获取中间键处理函数
     * @param $name
     * @return Closure|null
     */
    public static function get($name): ?Closure
    {
        if (empty($name) || false === strpos($name, '.')) {
            return null;
        }
        list($middleware, $method) = explode('.', $name);
        if (empty($method) || !array_key_exists($middleware, self::$middlewares)) {
            return null;
        }
        $middleware = self::$middlewares[$middleware];
        if (!method_exists($middleware, $method)) {
            return null;
        }
        return $middleware::$method();
    }

}