<?php
/**
 * Project Widget
 * file: Messenger.php
 * User: weblinuxgame
 * Date: 2019/5/27
 * Time: 17:20
 */

namespace WebLinuxGame\WeChat\Widget;

use Closure;
use EasyWeChat\Kernel\ServiceContainer;
use EasyWeChat\Kernel\Contracts\Arrayable;
use WebLinuxGame\WeChat\Widget\Exception\RuntimeException;

/**
 * 消息体
 * Class Messenger
 * @package WebLinuxGame\WeChat\Widget
 */
class Messenger implements Arrayable
{

    /**
     * @var Application|ServiceContainer $app
     */
    protected $app;
    protected $model = '';
    protected $message = [];
    protected $encrypt = false;

    const OPT_IN = ConstEnum::MESSENGER_OPT_IN; // 可选
    const MUST_IN = ConstEnum::MESSENGER_MUST_IN; // 必填

    // 消息数据导出类型支持
    protected static $attributes = ConstEnum::MESSENGER_MODEL_ATTRIBUTES;

    // 导出函数
    protected static $modelExport = [
        ConstEnum::MODEL_SEARCH => [self::class, 'getSearchData'],
        ConstEnum::MODEL_IMPORT => [self::class, 'getImportData'],
    ];

    /**
     * 构造消息对象
     * Messenger constructor.
     * @param array $message
     * @param ServiceContainer $app
     * @throws RuntimeException
     */
    public function __construct(array $message, ServiceContainer $app)
    {
        $this->message = $message;
        if (empty($app)) {
            throw new RuntimeException('miss widget application param', -1001);
        }
        $this->app = $app;
    }

    /**
     * 选择导出数据模型
     * @param string $model
     * @param bool $encrypt
     * @return Messenger
     * @throws RuntimeException
     */
    public function select(string $model, bool $encrypt = false): self
    {
        if (!array_key_exists($model, self::$attributes)) {
            throw new RuntimeException('value error select message model not support', -1002);
        }
        $this->model = $model;
        $this->encrypt = $encrypt;
        return $this;
    }

    /**
     * 外部数据重新载入
     * @param array $message
     * @return $this
     */
    public function data(array $message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * 加密
     * @param array $data
     * @return array
     */
    public function encrypt(array $data = []): array
    {
        if (empty($this->encrypt)) {
            return $data;
        }
        return $this->app[ConstEnum::PROVIDER_WIDGET_ENCRYPTOR]->encode(json_encode($data, JSON_UNESCAPED_UNICODE));
    }

    /**
     * 检查query 类型参数
     * @param $query
     * @return bool
     */
    protected function checkQueryParam($query)
    {
        if (empty($query)) {
            return false;
        }
        if (is_string($query)) {
            $query = @json_decode($query, true);
        }
        if (!is_array($query)) {
            return false;
        }
        // 类型检查
        if (is_array($query) &&
            (empty($query[ConstEnum::KEY_MSG_QUERY_TYPE]) || !is_numeric($query[ConstEnum::KEY_MSG_QUERY_TYPE]))) {
            return false;
        }
        if (1 !== count(array_keys($query))) {
            return false;
        }
        return true;
    }

    /**
     * 数组转换导出
     * @return array
     */
    public function toArray()
    {
        if (empty($this->model)) {
            return $this->getRow();
        }
        $handler = self::$modelExport[$this->model] ?? null;
        if (empty($handler)) {
            new RuntimeException("miss model {$this->model} export handler", -1004);
        }
        if (!$handler instanceof Closure && is_callable($handler)) {
            $handler = Closure::fromCallable($handler);
        }
        $this->checkQueryParam($this[ConstEnum::KEY_MSG_QUERY]);
        return $handler($this);
    }

    /**
     * Whether a offset exists
     * @link https://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return isset($this->message[$offset]);
    }

    /**
     * Offset to retrieve
     * @link https://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        return $this->message[$offset] ?? null;
    }

    /**
     * Offset to set
     * @link https://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        $this->message[$offset] = $value;
    }

    /**
     * Offset to unset
     * @link https://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        if (isset($this->message[$offset])) {
            unset($this->message[$offset]);
        }
    }

    /**
     * 获取查询输出格式数据
     * @param Messenger $messenger
     * @return array
     * @throws RuntimeException
     */
    public static function getSearchData(Messenger $messenger): array
    {
        if ($messenger->isEmpty()) {
            throw new RuntimeException('empty message', -1005);
        }
        $content = [];
        // model fields
        $attributesArr = $messenger->getModelAttributes();
        foreach ($attributesArr as $key => $opt) {
            $value = $messenger[$key];
            if (empty($value) && ConstEnum::KEY_MSG_TO_USERNAME == $key && !empty($messenger['toUserName'])) {
                $value = $messenger['toUserName'];
            }
            if (is_null($value)) {
                if (self::OPT_IN === $opt) {
                    $content[$key] = self::value($key);
                    continue;
                }
                new RuntimeException('miss message attribute' . $key, -1006);
            }
            $content[$key] = self::value($key, $value);
        }
        if (!empty($content['data'])) {
            $content['Content'] = $content['data'];
            unset($content['data']);
        }
        return $content;
    }

    /**
     * 获取数据导入格式消息数据
     * @param Messenger $messenger
     * @return array
     * @throws RuntimeException
     */
    public static function getImportData(Messenger $messenger): array
    {
        if ($messenger->isEmpty()) {
            throw new RuntimeException('empty message', -1005);
        }
        $content = [];
        // model fields
        $attributesArr = $messenger->getModelAttributes();
        foreach ($attributesArr as $key => $opt) {
            $value = $messenger[$key];
            if (is_null($value)) {
                if (self::OPT_IN === $opt) {
                    $content[$key] = self::value($key);
                    continue;
                }
                new RuntimeException('miss message attribute' . $key, -1006);
            }
            $content[$key] = self::value($key, $value);
        }
        return $messenger->encrypt($content);
    }

    /**
     * 值处理
     * @param string $key
     * @param null $data
     * @return false|int|null|string
     */
    protected static function value(string $key, $data = null)
    {
        if (ConstEnum::KEY_MSG_DATA === $key && is_array($data)) {
            return json_encode($data, JSON_UNESCAPED_UNICODE);
        }
        if (ConstEnum::KEY_MSG_QUERY === $key && !is_string($data)) {
            return json_encode($data, JSON_UNESCAPED_UNICODE);
        }
        if (ConstEnum::KEY_MSG_SCENE === $key && empty($data)) {
            return ConstEnum::VAL_DEF_SCENE;
        }
        if (ConstEnum::KEY_MSG_LIFESPAN === $key && empty($data)) {
            return ConstEnum::VAL_DEF_LIFESPAN;
        }
        return $data;
    }

    /**
     * 获取模型数据字段集合
     * @return array
     */
    public function getModelAttributes(): array
    {
        return self::$attributes[$this->model] ?? [];
    }

    /**
     * 获取原始数据
     * @return array
     */
    public function getRow(): array
    {
        return $this->message;
    }

    /**
     * 是否空
     * @return bool
     */
    public function isEmpty(): bool
    {
        if (empty($this->message)) {
            return true;
        }
        return false;
    }

}