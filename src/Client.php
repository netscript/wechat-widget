<?php
/**
 * Project Widget
 * file: Client* User: weblinuxgame
 * Date: 2019/5/24
 * Time: 18:02
 */

namespace WebLinuxGame\WeChat\Widget;


use EasyWeChat\Kernel\BaseClient;
use EasyWeChat\Kernel\ServiceContainer;


/**
 * 微信搜索Widget
 * Class Client
 * @package WebLinuxGame\WeChat
 */
class Client extends BaseClient
{
    // 是否使用https
    protected $sslHttp = true;

    // host domain
    protected $baseUri = ConstEnum::BASE_HOST_URL;

    // 解密查询
    protected static $encodeQuery = ConstEnum::CLIENT_ENCODE_QUERY_PARAMS;

    /**
     * 请求中间件
     */
    protected function registerHttpMiddlewares()
    {
        parent::registerHttpMiddlewares();
    }

    /**
     * 获取query 参数
     * @param array $message
     * @return array
     */
    protected static function getQuery(array &$message): array
    {
        $query = [];
        foreach (self::$encodeQuery as $key) {
            $value = $message[$key] ?? null;
            if (!empty($value)) {
                $query[$key] = $value;
                unset($message[$key]);
            }
        }
        return $query;
    }

    /**
     * 获取appid
     * @return string
     */
    protected function getAppId()
    {
        return $this->app[ConstEnum::KEY_CONFIG]->get(ConstEnum::KEY_APP_ID);
    }

    /**
     * 获取ese key
     * @return string
     */
    protected function getAesKey()
    {
        return $this->app[ConstEnum::KEY_CONFIG]->get(ConstEnum::KEY_WIDGET_AES);
    }

    /**
     * 是否启用https
     * @param bool $off
     * @return $this
     */
    public function setSsl(bool $off)
    {
        $this->sslHttp = $off;
        $this->getApiUri();
        return $this;
    }

    /**
     * 是否启用https
     * @return bool
     */
    public function getSsl(): bool
    {
        return $this->sslHttp;
    }

    /**
     * 获取请求url
     * @return string
     */
    public function getApiUri(): string
    {
        if ($this->sslHttp && preg_match('/^http:\/\//', $this->baseUri)) {
            $this->baseUri = str_replace('http:', 'https:', $this->baseUri);
        }
        if (!$this->sslHttp && preg_match('/^https:\/\//', $this->baseUri)) {
            $this->baseUri = str_replace('https:', 'http:', $this->baseUri);
        }
        return $this->baseUri;
    }

    /**
     * 请求结果判断是否成功
     * @param array $req
     * @return bool
     */
    public static function isSuccess(array $req): bool
    {
        if (empty($req)) {
            return false;
        }
        $code = (int)($req[ConstEnum::KEY_RES_ERR_CODE] ?? -1);
        $msg = (string)($req[ConstEnum::KEY_RES_ERR_MSG] ?? 'fail');
        if ((ConstEnum::VAL_RES_SUCCESS_CODE == $code) && ConstEnum::VAL_RES_SUCCESS_MSG == $msg) {
            return true;
        }
        return false;
    }

    /**
     * 是否加密
     * @param ServiceContainer $app
     * @param string $model
     * @return bool
     */
    public static function isEncrypt($app = null, string $model = ConstEnum::MODEL_SEARCH): bool
    {
        if (empty($app)) {
            return false;
        }
        if (!empty($app['request'])) {
            $flag = $app['request']->get('signature') && 'aes' === $app['request']->get('encrypt_type');
        }
        $aes = $app['config']->get(ConstEnum::KEY_WIDGET_AES) || $app['config']->get('aes_key');
        if (!empty($flag) && !empty($aes)) {
            return true;
        }
        if (!empty($aes) && ConstEnum::MODEL_SEARCH == $model) {
            return true;
        }
        return false;
    }

    /**
     * 转换
     * @param ServiceContainer|Application $app
     * @param string $model
     * @param array $message
     * @return array
     */
    public static function format($app, string $model = ConstEnum::MODEL_SEARCH, array $message = []): array
    {
        $oMessenger = $app[ConstEnum::PROVIDER_WIDGET_MESSENGER];
        if (empty($oMessenger)) {
            return $message;
        }
        $message = $oMessenger->data($message)->select($model, self::isEncrypt($app, $model))->toArray();
        return $message;
    }

    /**
     * 开发导入
     * @param array $data
     * @param string $query
     * @param string $toUserName
     * @param int $lifespan
     * @param int $scene
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     */
    public function import(array $data = [], $query = '', string $toUserName = '', $lifespan = ConstEnum::VAL_DEF_LIFESPAN, int $scene = 1)
    {
        $url = ConstEnum::IMPORT_URL_PATH;
        $message = compact('lifespan', 'query', 'data', 'toUserName', 'scene');
        $message = self::format($this->app, ConstEnum::MODEL_IMPORT, $message);
        if ('json' == $this->app[ConstEnum::KEY_CONFIG]->get(ConstEnum::KEY_REQUEST_TYPE)) {
            return $this->httpPostJson($url, $message);
        }
        return $this->httpPost($url, $message);
    }

    /**
     * 查询模板数据格式化[模板查询]
     * @param array $data
     * @param string|array $query
     * @param int $lifespan
     * @param int $scene
     * @return array
     */
    public function search(array $data = [], $query = '', $lifespan = ConstEnum::VAL_DEF_LIFESPAN, int $scene = 1): array
    {
        if (empty($data)) {
            return [];
        }
        $message = compact('lifespan', 'query', 'data', 'scene');
        return self::format($this->app, ConstEnum::MODEL_SEARCH, $message);
    }

}