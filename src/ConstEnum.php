<?php
/**
 * Project Widget
 * file: ConstEnump
 * User: weblinuxgame
 * Date: 2019/5/28
 * Time: 11:13
 */

namespace WebLinuxGame\WeChat\Widget;

/**
 * 包内常量
 * Interface ConstEnum
 * @package WebLinuxGame\WeChat\Widget
 */
interface ConstEnum
{
    const PROVIDER_REQ = 'request';
    const PROVIDER_WIDGET = 'widget';
    const PROVIDER_WIDGET_ENCRYPTOR = 'widget_encryptor';
    const PROVIDER_WIDGET_SERVER = 'server';
    const PROVIDER_WIDGET_MESSENGER = 'widget_messenger';
    const MODEL_SEARCH = 'search';
    const MODEL_IMPORT = 'import';

    const MSG_KEY_ENCRYPT = 'Encrypt';
    const MSG_KEY_NONCE = 'nonce';
    const MSG_KEY_TIMESTAMP = 'timestamp';
    const MSG_KEY_SIGNATURE = 'signature';
    const MSG_KEY_MSG_SIGNATURE = 'msg_signature';
    const KEY_ENCRYPT = 'Encrypt';
    const KEY_ACCESS_TOKEN = 'access_token';


    const KEY_QUERY = 'query';
    const KEY_REQUEST_TYPE = 'request_type';
    const KEY_WIDGET_AES = 'widget_aes_key';
    const KEY_APP_ID = 'app_id';
    const KEY_CONFIG = 'config';
    const KEY_TOKEN = 'token';

    const KEY_NONCE = 'Nonce';
    const KEY_TIMESTAMP = 'TimeStamp';
    const KEY_MSG_SIGNATURE = 'MsgSignature';
    const KEY_MSG_DATA = 'data';
    const KEY_MSG_SCENE = 'scene';
    const KEY_MSG_QUERY = 'query';
    const KEY_MSG_LIFESPAN = 'lifespan';
    const KEY_MSG_TO_USERNAME = 'ToUserName';
    const KEY_MSG_QUERY_TYPE = 'type';
    const KEY_RES_ERR_CODE = 'errcode';
    const KEY_RES_ERR_MSG = 'errmsg';
    const KEY_MSG_EVENT = 'Event';
    const KEY_MSG_TYPE = 'MsgType';
    const KEY_MSG_SQL = 'Query';

    const VAL_MSG_TYPE_EVENT = 'event';
    const VAL_MSG_WIDGET_EVENT = 'wxa_widget_data';
    const VAL_RES_SUCCESS_CODE = 0;
    const VAL_RES_SUCCESS_MSG = 'ok';
    const VAL_DEF_LIFESPAN = 86400;
    const VAL_DEF_SCENE = 1;

    const BASE_HOST_URL = 'https://api.weixin.qq.com/';
    const IMPORT_URL_PATH = 'wxa/setdynamicdata';

    const CLIENT_ENCODE_QUERY_PARAMS = [
        self::MSG_KEY_TIMESTAMP, self::MSG_KEY_NONCE, self::MSG_KEY_MSG_SIGNATURE, self::MSG_KEY_SIGNATURE,
    ];

    // Messenger 常量
    const MESSENGER_OPT_IN = 0;// 可选
    const MESSENGER_MUST_IN = 1; // 必填

    const MESSENGER_MODEL_ATTRIBUTES = [
        // 查询
        self::MODEL_SEARCH => [
            self::KEY_MSG_DATA => self::MESSENGER_MUST_IN,
            self::KEY_MSG_SCENE => self::MESSENGER_OPT_IN,
            self::KEY_MSG_QUERY => self::MESSENGER_MUST_IN,
            self::KEY_MSG_LIFESPAN => self::MESSENGER_MUST_IN,
            self::KEY_MSG_TO_USERNAME => self::MESSENGER_MUST_IN,
        ],
        // 开发导入
        self::MODEL_IMPORT => [
            self::KEY_MSG_DATA => self::MESSENGER_MUST_IN,
            self::KEY_MSG_SCENE => self::MESSENGER_OPT_IN,
            self::KEY_MSG_QUERY => self::MESSENGER_MUST_IN,
            self::KEY_MSG_LIFESPAN => self::MESSENGER_MUST_IN,
        ],
    ];

    const ERR_CODE_EMPTY_DATA_PUSH = 1404; // 空推送
    const ERR_CODE_ERROR_PARAM_QUERY = 1500; // 异常query
}