<?php
/**
 * Project Widget
 * file: ServiceProvider.php
 * User: weblinuxgame
 * Date: 2019/5/27
 * Time: 10:27
 */

namespace WebLinuxGame\WeChat\Widget;


use Closure;
use Pimple\Container;
use EasyWeChat\OfficialAccount\Server\Handlers\EchoStrHandler;
use EasyWeChat\MiniProgram\Server\ServiceProvider as BaseServiceProvider;

/**
 * 服务提供
 * Class ServiceProvider
 * @package WebLinuxGame\WeChat
 */
class ServiceProvider extends BaseServiceProvider
{

    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        // widget encryptor
        $this->add($app, ConstEnum::PROVIDER_WIDGET_ENCRYPTOR, function ($app) {
            return new Encryptor(
                $app[ConstEnum::KEY_CONFIG][ConstEnum::KEY_APP_ID],
                $app[ConstEnum::KEY_CONFIG][ConstEnum::KEY_TOKEN],
                $app[ConstEnum::KEY_CONFIG][ConstEnum::KEY_WIDGET_AES]
            );
        });
        // widget client
        $this->add($app, ConstEnum::PROVIDER_WIDGET, function ($app) {
            return new Client($app, $app[ConstEnum::KEY_ACCESS_TOKEN]);
        });

        // widget messenger
        $this->add($app, ConstEnum::PROVIDER_WIDGET_MESSENGER, function ($app) {
            return new Messenger([], $app);
        });

        // widget server
        $this->add($app, ConstEnum::PROVIDER_WIDGET_SERVER, function ($app) {
            $guard = new Guard($app);
            $guard->push(new EchoStrHandler($app));
            return $guard;
        },true);
        parent::register($app);
    }

    /**
     * 添加服务
     * @param Container $app
     * @param string $key
     * @param Closure $maker
     * @param bool $force
     */
    protected function add(Container &$app, string $key, Closure $maker,bool $force = false)
    {
        (!isset($app[$key]) || $force) && $app[$key] = $maker;
    }
}