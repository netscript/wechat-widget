<?php
/**
 * Project Widget
 * file: Encryptor.php
 * User: weblinuxgame
 * Date: 2019/5/25
 * Time: 19:09
 */

namespace WebLinuxGame\WeChat\Widget;

use Throwable;
use EasyWeChat\Kernel\Support\AES;
use EasyWeChat\MiniProgram\Encryptor as BaseEncryptor;
use WebLinuxGame\WeChat\Widget\Exception\RuntimeException;

/**
 * Widget 加密解密
 * Class Encryptor
 * @package WebLinuxGame\WeChat
 */
class Encryptor extends BaseEncryptor
{

    /**
     * 加密数据
     * @param $replyMsg
     * @param string $nonce
     * @param int|null $timestamp
     * @return array
     * @throws RuntimeException
     */
    public function encode($replyMsg, string $nonce = null, int $timestamp = null): array
    {
        try {
            $row = $this->pkcs7Pad(str_random(16) . pack('N', strlen($replyMsg)) . $replyMsg . $this->appId, $this->blockSize);

            $encrypted = base64_encode(AES::encrypt(
                $row,
                $this->aesKey,
                substr($this->aesKey, 0, 16),
                OPENSSL_NO_PADDING
            ));
        } catch (Throwable $e) {
            throw new RuntimeException($e->getMessage(), self::ERROR_ENCRYPT_AES);
        }

        !is_null($nonce) || $nonce = substr($this->appId, 0, 10);
        !is_null($timestamp) || $timestamp = time();
        $response = [
            ConstEnum::KEY_ENCRYPT => $encrypted,
            ConstEnum::KEY_MSG_SIGNATURE => $this->signature($this->token, $timestamp, $nonce, $encrypted),
            ConstEnum::KEY_TIMESTAMP => $timestamp,
            ConstEnum::KEY_NONCE => $nonce,
        ];
        return $response;
    }

    /**
     * 解密数据
     * @param $content
     * @param $msgSignature
     * @param $nonce
     * @param $timestamp
     * @return string
     * @throws \EasyWeChat\Kernel\Exceptions\RuntimeException
     */
    public function decode($content, $msgSignature, $nonce, $timestamp): string
    {
        return $this->decrypt($content, $msgSignature, $nonce, $timestamp);
    }

}